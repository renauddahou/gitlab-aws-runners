# Gitlab CI on AWS

This repo contains code to fully automate the process of setting up Gitlab Runners on **AWS Fargate** or on a **single EC2 instance**. For both setups, there's a deployment script and a cloudformation template available.

Contents:
1. Gitlab runners on AWS Fargate
2. Gitlab runners on AWS EC2
3. Deployment

## 1. Gitlab runners on AWS Fargate
The approach and how to set up manually is described at [Gitlab Docs: Autoscale on AWS Fargate](https://docs.gitlab.com/runner/configuration/runner_autoscale_aws_fargate/).
The deployment is now fully automated and the approach is extended with the following improvements:
* EC2 instance doesn't expose public access or even ports within the VPC. The sole access happens via AWS Systems Manager (SSM).
* Fargate Runners will only accept SSH connections coming from the EC2 instance's IP.
* If the Fargate Task Definition defined a Task Execution role, the container will propagate the resulting AWS credentials to the SSH session via environment variables.
* From the original code at [gitlab.com/tmaczukin-test-projects/fargate-driver-debian](https://gitlab.com/tmaczukin-test-projects/fargate-driver-debian), the Fargate container's Debian and gitlab-runner versions are updated.

### Debian Buster based image for usage with AWS Fargate Custom Executor

'Dockerfile' comprises an image ready to run the simplest jobs with the [AWS Fargate Custom Executor driver](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate). It' doesn't provide
any custom tooling nor language support.

But it will be more than enough to handle `echo "Hello world"` job properly!
This image needs to be used as a base for your Fargate workers. You may want to inherit from it or modify to add custom language or tooling support.


# 2. Gitlab runners on AWS EC2
This setup is suitable for tiny projects, since it uses a single EC2 instance to run gitlab CI containers. This enables to run docker containers in privileged mode, which is not possible on AWS Fargate. While this can cause security issues, it enables the use of podman and other tools to build or run containers within Gitlab CI.
##### Notes:
* There might be security implications running docker in privileged mode
* The docker image must provide an installation of curl and jq

### Obtaining the EC2 instances iam permissions
AWS administrative credentials (from the EC2 instance profile, see cloudformation template) are automatically assigned to every shell running on the EC2 instance.

However, since the gitlab runner will spin up new docker containers for each CI job, those containers do not get authenticated with AWS out of the box. Luckily, we can [fetch temporary credentials via the EC2 instance's local network](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instancedata-data-retrieval.html). For this to work, you'll need to add the following lines to the script part of your .gitlab-ci.yml file. After obtaining temporary credentials, you may use boto3 or the aws cli with the role and permissions granted to the EC2 instance.
```yaml
- 'TOKEN=$(curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600")'
- 'ROLE=$(curl -H "X-aws-ec2-metadata-token: $TOKEN" -v http://169.254.169.254/latest/meta-data/iam/security-credentials/)'
- 'CREDENTIALS=$(curl -H "X-aws-ec2-metadata-token: $TOKEN" -v http://169.254.169.254/latest/meta-data/iam/security-credentials/$ROLE)'
- AWS_ACCESS_KEY_ID=$(echo "$CREDENTIALS" | jq -r '.AccessKeyId')
- AWS_SECRET_ACCESS_KEY=$(echo "$CREDENTIALS" | jq -r '.SecretAccessKey')
- AWS_SESSION_TOKEN=$(echo "$CREDENTIALS" | jq -r '.Token')
- aws sts get-caller-identity || (echo "No valid AWS credentials found" && exit 255)
```

# 3. Deployment
Prior to deployment, make sure to understand and adapt the necessary parameters in the cloudformation template of your choice.

The deploy_*.sh scripts build and push the base docker image and deploy the required AWS resources: EC2 Instance, Fargate Cluster, Fargate Workers, Security Groups, ECR, ...
Prior to running the deployment, make sure you have assumed an administrative role on your console using ```$ aws sts assume role ...```.
Usage:
```bash
./deploy_xx.sh <Gitlab runner registration token>
```
You can obtain your Gitlab runner token from Gitlab web UI.
