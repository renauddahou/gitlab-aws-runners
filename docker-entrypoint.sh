#!/bin/bash

# Create a folder to store user's SSH keys if it does not exist.
USER_SSH_KEYS_FOLDER=~/.ssh
[ ! -d ${USER_SSH_KEYS_FOLDER} ] && mkdir -p ${USER_SSH_KEYS_FOLDER}

# Copy contents from the `SSH_PUBLIC_KEY` environment variable
# to the `$USER_SSH_KEYS_FOLDER/authorized_keys` file.
# The environment variable must be set when the container starts.
echo ${SSH_PUBLIC_KEY} > ${USER_SSH_KEYS_FOLDER}/authorized_keys

# Clear the `SSH_PUBLIC_KEY` environment variable.
unset SSH_PUBLIC_KEY

if [ -n "$AWS_CONTAINER_CREDENTIALS_RELATIVE_URI" ] ; then
  CREDENTIALS=$(curl "169.254.170.2$AWS_CONTAINER_CREDENTIALS_RELATIVE_URI")
  AWS_ACCESS_KEY_ID=$(echo "$CREDENTIALS" | jq '.AccessKeyId')
  AWS_SECRET_ACCESS_KEY=$(echo "$CREDENTIALS" | jq '.SecretAccessKey')
  AWS_SESSION_TOKEN=$(echo "$CREDENTIALS" | jq '.Token')

  echo "
  export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
  export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
  export AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN
  " >> /etc/environment
fi

# Start the SSH daemon
exec /usr/sbin/sshd -D
